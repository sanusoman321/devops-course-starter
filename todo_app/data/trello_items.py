import requests, json, os
from todo_app.data.todo_items import Item
# from dotenv import load_dotenv
# load_dotenv()

def base_url():
    url = os.getenv("TRELLO_BASEURL")
    return url
def board_id():
    board = board_id = os.getenv("TRELLO_BOARDID")
    return board
# base_url = os.getenv("TRELLO_BASEURL")
# board_id = os.getenv("TRELLO_BOARDID")
# trello_key = os.getenv("API_KEY")
# trello_token = os.getenv("API_TOKEN")

def get_lists():
    params = {
        'key' : os.getenv("API_KEY"),
        'token' : os.getenv("API_TOKEN"),
        'cards': 'open'
    }
    print(params)
    url = f'{base_url()}/boards/{board_id()}/lists'
    response = requests.get(url, params = params)
    lists = response.json()
    return lists
    # print(lists)

def get_list(name):
    lists = get_lists()
    return next((list for list in lists if list['name'] == name), None)

def get_items():
    lists = get_lists()
    items = []
    for card_list in lists:
        for card in card_list['cards']:
            items.append(Item.fromTrelloCard(card, card_list))
    return items

def add_item(name, desc, due):
    todo_list = get_list('Not Started')
    url = f'{base_url}/cards'
    params = {
        'key': trello_key,
        'token': trello_token,
        'name': name,
        'desc': desc,
        'due': due,
        'idList': todo_list['id']
        }
    print(todo_list)
    response = requests.post(url, params = params)
    print(response)
    card = response.json()
    return Item.fromTrelloCard(card, todo_list)

def move_card_to_list(card_id, list):
    url = f'{base_url}/cards/{card_id}'
    params = {
        'key': trello_key,
        'token': trello_token,
        'idList': list['id']
        }
    response = requests.put(url, params=params)
    card = response.json()
    return card

def item_in_progress(id):
    doing_list = get_list('In Progress')
    card = move_card_to_list(id, doing_list) 
    return Item.fromTrelloCard(card, doing_list)

def item_completed(id):
    done_list = get_list('Completed')
    card = move_card_to_list(id, done_list)
    return Item.fromTrelloCard(card, done_list)

def reset_item_status(id):
    todo_list = get_list('Not Started')
    card = move_card_to_list(id,todo_list)
    return Item.fromTrelloCard(card, todo_list)



