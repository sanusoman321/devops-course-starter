class ViewModel:
    def __init__(self, items):
        self._items = items

## Index function returning list of all items
    @property
    def items(self):
        return self._items

@property
def doing_items(self):
    doing_items = []
    for item in self._items:
        if item.status == "Not Started":
            doing_items.append(item)
        if item.status == "In Progress":
            doing_items.append(item)
        return doing_items
