from todo_app.data.view_model import ViewModel
from todo_app.data.todo_items import Item

def test_doing_items():
    model_data = [Item(id="123", short_id="321", name="Test5", desc="desc", due="2022-2-16", status="In Progress")]
    test_view = ViewModel(model_data)
    assert test_view.items[0].id  == "123"
    assert test_view.items[0].short_id  == "321"
    assert test_view.items[0].name  == "Test5"
    assert test_view.items[0].due  == "2022-2-16"
    assert test_view.items[0].status  == "In Progress"

def test_done_items():
    model_data = [Item(id="1234", short_id="3210", name="Test6", desc="desc", due="2022-2-17", status="Completed")]
    test_view = ViewModel(model_data)
    assert test_view.items[0].id  == "1234"
    assert test_view.items[0].short_id  == "3210"
    assert test_view.items[0].name  == "Test6"
    assert test_view.items[0].due  == "2022-2-17"
    assert test_view.items[0].status  == "Completed"

def test_to_do_items():
    model_data = [Item(id="12345", short_id="32100", name="Test7", desc="desc", due="2022-2-18", status="Not Started")]
    test_view = ViewModel(model_data)
    assert test_view.items[0].id  == "12345"
    assert test_view.items[0].short_id  == "32100"
    assert test_view.items[0].name  == "Test7"
    assert test_view.items[0].due  == "2022-2-18"
    assert test_view.items[0].status  == "Not Started"


