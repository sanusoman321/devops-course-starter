FROM python:3.10.4 as base
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN pip install poetry && poetry install

FROM base as development
COPY . /app
EXPOSE 5000/tcp
RUN poetry install
CMD ["poetry", "run", "flask", "run", "-h", "0.0.0.0"]

FROM base as production
COPY . /app
EXPOSE 8000/tcp
RUN poetry install
CMD ["poetry", "run", "gunicorn", "-b", "0.0.0.0", "todo_app.app:create_app()"]

FROM base as test
COPY . /app
RUN poetry install
CMD ["poetry", "run", "pytest"]





