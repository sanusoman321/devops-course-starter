# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.7+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py -UseBasicParsing).Content | python -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Creating Trello account and API

1. Create new account in trello by following sign-up process via https://trello.com/signup
2. Genrate Trello API token by following the link https://trello.com/app-key
3. Create new board and add following lists like "Not Started", "In Progress", "Completed"
4. Update api key, api token, trello base url and trello board ID as enviorment vaiable in .env file 

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Testing to-do app
1. Create view_model file for holding all of the items in to-do list
2. Modify app.py to import ViewModel class
3. Modify index.html file
4. Add pytest dependencies
5. Add tests to test_app.py and validate
6. Create app function and a new file for environment variables for integration tests.
7. Create test client
8. To execute test - **poetry run pytest**

## Using ansible to deploy and configure application on application AWS host
1. Configure passwordless ssh connection with both Ansible controller and node host
2. Install ansible if not available on controller node
3. Copy todo app to the controller node home directory
4. Configure ansible playbook and inventory file with necessary configurations
5. Create .env.j2 file with environment variables
6. Create todoapp service file for configure toadapp service as systemd
7. Run ansible using command - **ansible-playbook ansible-playbook.yml -i inventory.ini**
8. Check application status in browser via **http://<nodehost_ip>:5000**

## Run application using container image for dev and prod and for tests as well
1. Create dockerfile for development environement
2. Build docker image using **docker build --target development --tag todo-app:dev .**
3. Run using **docker run --env-file ./.env -p 5000:5000 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app todo-app:dev**
4. Verify the container by accessing http://localhost:5000
5. Modify dockerfile for production environment
6. Make sure gunicorn depenendcies are added via "poetry install gunicorn"
7. Then, build the prod container using **docker build --target production --tag todo-app:prod .**
8. Run the prod container using **docker run --env-file ./.env -p 8000:8000 -it  todo-app:prod**
9. Verify it by accessing http://localhost:8000
10. Add pytest related to dockerfile
11. Build the test enviornment using **docker build --target test --tag todo-app:test .**
12. Run the test using **docker run todo-app:test**
13. Create a docker-compose.yaml file and add dev, prod and test enviornment details
14. Run the compse command to verify it **docker-compose up**

